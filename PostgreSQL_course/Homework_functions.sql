-- Homework 8
-- Functions

DROP FUNCTION customers_back_up();
CREATE OR REPLACE FUNCTION customers_back_up() RETURNS void AS 
$$
	CREATE TABLE customers_copy AS
	SELECT *
	FROM customers;
$$ LANGUAGE SQL; --1. Create function which create copy of customers table

 SELECT * FROM customers_back_up();
 SELECT * FROM customers_copy; --1. Create function which create copy of customers table
 
SELECT * FROm products;
 
 
CREATE OR REPLACE FUNCTION avg_freight() RETURNS real AS
$$
	SELECT AVG(freight)
	FROM orders;

$$ LANGUAGE SQL;
 
 
SELECT avg_freight(); -- 2. Create function which return average of freight column in orders table


CREATE OR REPLACE FUNCTION task_3(num1 int , num2 int) RETURNS real AS
$$
DECLARE
	ans real;
BEGIN
	ans = (num2 - num1+1)*random()+num1;
	RETURN floor(ans);
END;
$$ LANGUAGE plpgsql;

SELECT task_3(1,5);
 
 
SELECT *
FROM employees;


CREATE FUNCTION min_max_employee_salary(employee_city varchar, OUT min_salary int, OUT max_salary int) AS
$$
	SELECT MIN(salary), MAX(salary)
	FROM employees
	WHERE city = employee_city;

$$ LANGUAGE SQL;


SELECT min_salary, max_salary
FROM min_max_employee_salary('London');



CREATE OR REPLACE FUNCTION change_salary(con real DEFAULT 0.15, high_salary double precision  DEFAULT 15000 )
RETURNS void AS
$$
	UPDATE employees
	SET salary = salary + salary*con
	WHERE salary <= high_salary;
$$ LANGUAGE SQL;



SELECT change_salary(0.2);

SELECT salary
FROm employees;



DROP FUNCTION change_salary_2;
CREATE OR REPLACE FUNCTION change_salary_2(con real DEFAULT 0.15, high_salary double precision  DEFAULT 15000 )
RETURNS SETOF employees AS
$$
	UPDATE employees
	SET salary = salary + salary*con
	WHERE salary <= high_salary
	RETURNING *;
$$ LANGUAGE SQL;


SELECT *
FROM change_salary_2();



CREATE OR REPLACE FUNCTION change_salary_3(con real DEFAULT 0.15, high_salary double precision  DEFAULT 15000 )
RETURNS TABLE(f_name varchar, l_name varchar, title varchar, salary double precision) AS
$$
	UPDATE employees
	SET salary = salary + salary*con
	WHERE salary <= high_salary
	RETURNING first_name, last_name, title,  salary;
$$ LANGUAGE SQL;


SELECT *
FROM change_salary_3();



CREATE FUNCTION check_salary(c_salary double precision, 
							 max_salary double precision DEFAULT 15000, 
							 min_salary double precision DEFAULT 10000,
							 cof decimal DEFAULT 0.2)
							 RETURNS bool AS
$$
DECLARE 
	ans bool;
BEGIN
		IF c_salary > min_salary THEN
				ans := false;
		ELSIF c_salary <= min_salary THEN
			c_salary := c_salary + c_salary*cof;
			IF c_salary > max_salary THEN
				ans := false;
			ELSE
				ans := true;
			END IF;
		END IF;
	RETURN ans;
END;
$$ LANGUAGE plpgsql;
					
SELECT check_salary(9000);	
