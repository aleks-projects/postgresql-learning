-- Lesson 20
-- Functions 


CREATE OR REPLACE FUNCTION average_price_group_by_category_id() RETURNS SETOF double precision
AS
$$
SELECT AVG(unit_price)
FROM products
GROUP BY category_id
$$
LANGUAGE SQL; -- function return one column as average price grouped by category_id

SELECT average_price_group_by_category_name();





CREATE OR REPLACE FUNCTION averange_price_with_category_name()
RETURNS TABLE (category_name varchar(15), average_price double precision) AS
$$
SELECT category_name, AVG(unit_price)
FROM products
INNER JOIN categories USING(category_id)
GROUP BY category_name
$$
LANGUAGE SQL; -- function averange_price_with_category_name() return table with two columns category_name and
-- average_price, function combine two tables products and categories using INNER JOIN and category_id key
-- average_price is grouped by category_name

SELECT category_name, average_price
FROM averange_price_with_category_name();





CREATE OR REPLACE FUNCTION average_sum_by_category_id(OUT price_sum real, OUT average_price double precision)
	RETURNS SETOF RECORD AS
	$$
	SELECT SUM(unit_price), AVG(unit_price)
	FROM products
	GROUP BY category_id
	$$
	LANGUAGE SQL; -- RETURNS SETOF RECORD AS - is using tu return many rows if you delete RETURNS SETOF RECORD AS you can 
	--return only one row


SELECT price_sum
FROM average_sum_by_category_id();


SELECT price_sum, average_price
FROM average_sum_by_category_id();






CREATE OR REPLACE FUNCTION  name_country(country_c varchar) RETURNS TABLE(cust_id char, cust_name varchar) AS
$$
SELECT customer_id, contact_name
FROM customers
WHERE country = country_c;
$$
LANGUAGE SQL;


SELECT cust_id, cust_name
FROM name_country('France');


SELECT cust_name
FROM name_country('France');
