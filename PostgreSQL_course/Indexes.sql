-- Lesson 25
-- Indexes

SELECT amname 
FROM pg_am; -- select all idexes which can be used

-- index btree is used Deafult

-- CREATE INDEX index_name ON table_name USING HASH (columns_name); -- create hash ind ex 


EXPLAIN SELECT unit_price
FROM products; -- show plan of quyery
 

EXPLAIN ANALYZE SELECT unit_price
FROM products; -- show plan of query and qury details


-- Practice indexes


CREATE TABLE perf_test
(
	id int,
	reason text COLLATE "C",
	annotation text COLLATE "C"
); -- creating table perf_test

INSERT INTO perf_test(id,reason,annotation)
SELECT s.id, md5(random()::text), null
FROM generate_series(1,10000000) AS s(id)
ORDER BY random();

UPDATE perf_test
SET annotation = UPPER(md5(random()::text)); -- filling the table with random data 


SELECT *
FROM perf_test
LIMIT 10; -- select first 10 records from table per_test


EXPLAIN
SELECT *
FROM perf_test
WHERE id = 3700000; -- select query plan



CREATE INDEX idx_perf_test_id ON perf_test(id); -- creat index 'idx_perf_test_id' on table perf_test and column id



--Before creating index query: SELECT *
--							   FROM perf_test
--							   WHERE id = 3700000; lasted about 300 msec after creating index the query lasted about 70 msec

EXPLAIN ANALYZE
SELECT * 
FROM perf_test
WHERE reason LIKE 'bc%' AND annotation LIKE 'AB%';


CREATE INDEX idx_per_test_reason_annotation ON perf_test(reason, annotation);



EXPLAIN
SELECT *
FROM perf_test
WHERE reason LIKE 'ae%'; -- now select in only first  column(reason) just like select in two columns use index scan


EXPLAIN
SELECT * 
FROM perf_test
WHERE annotation LIKE 'FD%'; -- this query use seq scan

CREATE INDEX idx_perf_test_annotation ON perf_test(annotation); -- create other index only for column annotation




EXPLAIN
SELECT * 
FROM perf_test
WHERE LOWER(annotation) LIKE 'ab%'; -- this query does not use idex scan you need to create other index


CREATE INDEX idx_perf_test_annotation_lower ON perf_test(LOWER(annotation)); -- now above query use index scan
