--LESSON 5:
--LIKE, LIMIT, NULL, IS NULL, IS NOT NULL
-- LIKE - templates:
-- '%' - 0,1 or more symbols
-- '_' - 1 exactly one symbol
-- LIKE 'U%' - text beggining from U
-- LIKE '%a' - text ending from a
-- LIKE '%John%' - text with containing John
-- LIKE 'J%n' - text begging with J and ending from n
-- LIKE '_oh_' - text in which 2 and 3 symbols are 'o' and 'h', 1 and 4 are  arbitrary 

SELECT first_name, last_name
FROM employees
WHERE first_name LIKE '%n'; -- select columns first_name, last_name from table employees where first_name ending from n


SELECT first_name, last_name
FROM employees
WHERE last_name LIKE 'B%'; -- select columns first_name, last_name from table employees where last_name beggining  from n


SELECT first_name, last_name
FROM employees
WHERE last_name LIKE '_uch%'; -- select columns first_name, last_name from table employees where last_name begging from arbitrary symbol
--and 2,3,4 symbols are is 'uch' 

SELECT product_name, unit_price
FROM products
WHERE unit_price > 20 and unit_price <40
LIMIT 10; -- select columns product_name, unit_price from table products where unit_price is greaten than 20 and less than 40.
-- SELECT ONLY first 10 rows - LIMI function

SELECT shipped_date, ship_city, ship_region
FROM orders
WHERE ship_region IS NULL; --select columns: shipped_date, ship_city, ship_region from table orders where ship_region is NULL


SELECT shipped_date, ship_city, ship_region
FROM orders
WHERE ship_region IS NOT NULL; --select columns: shipped_date, ship_city, ship_region from table orders where ship_region is not NULL
