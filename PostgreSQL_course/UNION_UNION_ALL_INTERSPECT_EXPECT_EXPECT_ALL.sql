--LESSON 7
-- UNION, UNION ALL, INTERSECT, EXPECT, EXPECT ALL

SELECT country
FROM employees
UNION
SELECT country
FROM customers; -- select column country from tables employees and customers and combine it to one select without duplicates


SELECT country
FROM employees
UNION ALL 
SELECT country
FROM customers; -- select column country from tables employees and customers and combine it to one select with duplicates


SELECT country
FROM customers
INTERSECT
SELECT country
FROM suppliers; -- select column country from tables customers and suppliers if country includes at the same time in the table customers and suppliers 


SELECT country 
FROM customers
EXCEPT
SELECT country
FROM suppliers; -- select column country from table customers if country is not in suppliers table


SELECT country
FROM customers
EXCEPT ALL
SELECT country
FROM suppliers; -- select column country from table customers if country is not in supllier table or if table customers have more duplicates of country
-- than suppliers table
