--Homework 6
-- 'DDL' homework

DROP TABLE exam;

CREATE TABLE exam
(
	exam_id serial UNIQUE NOT NULL,
	exam_name varchar(128),
	exam_date date
	
); -- 1. Create an exam table with the following columns:
-- exam identifier - auto-incremented, unique, forbid NULL;- exam name - exam date

ALTER TABLE exam 
DROP CONSTRAINT exam_exam_id_key; -- 2. Remove the uniqueness restriction from the identifier field

ALTER TABLE exam 
ADD CONSTRAINT PK_exam_exam_id PRIMARY KEY(exam_id); -- 3. Add a primary key constraint on the identifier field



CREATE TABLE person
(
	person_id int,
	fisrt_name varchar(128),
	last_name varchar(128),
	
	CONSTRAINT PK_person_person_id PRIMARY KEY (person_id)
	
); -- 4. Create a table person with the following columns
-- person identifier (simple int, primary key)- first name - last name


CREATE TABLE passport
(
	passport_id int,
	serial_num int NOT NULL,
	registr text NOT NULL,
	person_id int NOT NULL,
	
	CONSTRAINT PK_passport_passport_id PRIMARY KEY (passport_id),
	CONSTRAINT FK_person_passport_id FOREIGN KEY (person_id) REFERENCES person(person_id)
	
); --5. Create a passport table with columns:
-- passport identifier (simple int, primary key)
-- serial number (simple int, forbid NULL)- registration- reference to person_id (foreign key)


ALTER TABLE book
ADD COLUMN weight
DECIMAL CONSTRAINT CHK_weight
CHECK (weight > 0 AND weight <100); -- 6. Add a weight column to the book table (created earlier) with a constraint 
--that checks the weight (greater than 0 but less than 100).



INSERT INTO book
VALUES
(1,'Harry Potter', '1234567',1,150); --7. Verify that the weight limit works (try inserting a non-valid value)


CREATE TABLE IF NOT EXISTS student
(
	student_id int GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT 1),
	full_name text NOT NULL,
	course int  DEFAULT 1

); -- 8. Create a student table with the columns:
-- identifier (autoincrement)
--full name
--course (default 1)



INSERT INTO student (full_name)
VALUES
('John Brown');
--9. Insert an entry into the student table and verify that the default value insertion constraint works
SELECT * FROM student;


ALTER TABLE student
ALTER COLUMN course DROP DEFAULT; -- 10. Remove the "default" restriction from the student table


ALTER TABLE products
ADD CONSTRAINT CHK_unit_price CHECK (unit_price > 0); --11. Connect to the northwind database and add a constraint
--on the unit_price field of the products table (the price must be greater than 0)



SELECT MAX(product_id) FROM products;

CREATE SEQUENCE IF NOT EXISTS products_product_id_seq
START WITH 78 OWNED BY products.product_id;



ALTER TABLE products
ALTER COLUMN product_id SET DEFAULT nextval('products_product_id_seq');-- 12. "Hang" auto-incrementing counter
--on the product_id field of the products table (northwind database).
--The counter should start from the number following the maximum value of this column


SELECT * FROM products;

INSERT INTO products(product_name, supplier_id, category_id, quantity_per_unit,
			unit_price, units_in_stock, units_on_order, reorder_level, discontinued)
VALUES  
('Bananas',1,1,'1kg',23,100,15,1,0)
RETURNING product_id; -- 13. Insert into products (without inserting the identifier explicitly) and make sure 
--that autoincrement works.
--Make the insertion so that the command returns the value generated as an identifier.
