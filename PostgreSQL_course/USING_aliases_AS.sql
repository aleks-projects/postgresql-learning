--LESSON 11
--USING , aliases


SELECT contact_name, company_name, phone, first_name, last_name, title,
	order_date, product_name, ship_country, products.unit_price, quantity, discount
FROM orders
JOIN order_details ON orders.order_id = order_details.order_id
JOIN products ON order_details.product_id = products.product_id
JOIN customers ON orders.customer_id = customers.customer_id
JOIN employees ON orders.employee_id = employees.employee_id
WHERE ship_country = 'USA'; 



SELECT contact_name, company_name, phone, first_name, last_name, title,
	order_date, product_name, ship_country, products.unit_price, quantity, discount
FROM orders
JOIN order_details USING(order_id)--ON orders.order_id = order_details.order_id
JOIN products USING(product_id)--ON order_details.product_id = products.product_id
JOIN customers USING(customer_id)--ON orders.customer_id = customers.customer_id
JOIN employees USING(employee_id)--ON orders.employee_id = employees.employee_id
WHERE ship_country = 'USA'; -- the same as above example


--Aliases


SELECT COUNT(*)
FROM employees;

SELECT COUNT(*) AS employees_count
FROM employees;


SELECT category_id, SUM(units_in_stock*unit_price) AS total_price
FROM products
WHERE units_in_stock > 20 
GROUP BY category_id
HAVING SUM(units_in_stock*unit_price) > 5000 -- cant use total_price in filters such as WHERE, HAVING
ORDER BY total_price DESC
LIMIT 10;
