--LESSON 13
-- WHERE EXISTS, ANY, ALL


SELECT company_name, contact_name
FROM customers
WHERE EXISTS (SELECT customer_id
			 FROM orders
			 WHERE customer_id = customers.customer_id AND freight BETWEEN 50 AND 100);
			 
			 
SELECT company_name, contact_name
FROM customers
WHERE NOT EXISTS (SELECT customer_id
			 FROM orders
			 WHERE customer_id = customers.customer_id AND order_date BETWEEN '1995-02-01' AND '1995-02-15');


SELECT product_name, product_id
FROM products
WHERE NOT EXISTS (SELECT order_id 
				FROM orders
				JOIN order_details USING(order_id)
				WHERE order_details.product_id = products.product_id AND order_date BETWEEN '1995-02-01' AND '1995-02-15');
				
				
-- ANY, ALL

SELECT DISTINCT company_name
FROM customers
JOIN orders USING(customer_id)
JOIN order_details USING(order_id)
WHERE quantity > 40;


SELECT DISTINCT company_name
FROM customers
WHERE customer_id = ANY (
						SELECT customer_id
						FROM orders
						JOIN order_details USING(order_id)
						WHERE quantity > 40
						);


SELECT DISTINCT product_name, quantity
FROM products
JOIN order_details USING(product_id)
WHERE quantity > (SELECT AVG(quantity)
				  FROM order_details)
ORDER BY quantity;




SELECT DISTINCT product_name, quantity
FROM products
JOIN order_details USING(product_id)
WHERE quantity > ALL (
					SELECT AVG(quantity)
					FROM order_details
					GROUP BY product_id)
ORDER BY quantity;
