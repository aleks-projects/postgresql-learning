--LESSON 3:
--BETWEEN, IN, NOT IN, ORDER BY

SELECT order_date, order_id, freight 
FROM orders
WHERE freight
BETWEEN 20 AND 40; -- select columns: order_date, order_id, freight from table orders where freight is between 20 and 40


SELECT order_date, order_id, freight 
FROM orders
WHERE freight >= 20 AND freight <= 40; -- same as above


SELECT *
FROM orders
WHERE order_date
BETWEEN '1996-07-04' AND '1996-07-15'; -- select all columns from table orders where order_date is between '1996-07-04' adn '1996-07-15'



SELECT company_name, address, city, country
FROM customers
WHERE country ='USA' OR country='Canada' OR country='Mexico'; -- select columns:company_name, address, city, country from table customers
-- where country is USA or Canada or Mexico


SELECT company_name, address, city, country
FROM customers
WHERE country IN ('USA', 'Canada', 'Mexico'); --same as above


SELECT *
FROM products
WHERE category_id IN (1, 3, 5, 7); -- select all columns from table products where category_id is 1 or 3 or 5 or 7


SELECT company_name, address, city, country
FROM customers
WHERE country NOT IN ('USA', 'Canada', 'Mexico'); -- select columns:company_name, address, city, country from table customers
-- where country is not USA or Canada or Mexico


SELECT DISTINCT country
FROM customers; -- select only unique countries from column country from table customers;

SELECT DISTINCT country
FROM customers
ORDER BY country; -- select only unique countries from column country from table customers and sort all elements in ascending order (ASC) ;


SELECT DISTINCT country
FROM customers
ORDER BY country DESC; -- select only unique countries from column country from table customers and sort all elements in descending order (DESC) ;


SELECT DISTINCT country, city
FROM customers
ORDER BY country DESC, city ; -- select only unique pairs country-city from column country from table customers
-- and sort all elements from country column in descending order (DESC) and sort all elements from city column in ascending order (ASC) ;
