--Homework 2:
SELECT * 
FROM orders
WHERE ship_country IN ('France', ' Austria', 'Spain'); -- Select all orders from France, Austria, Spain



SELECT * 
FROM orders
ORDER BY required_date DESC, shipped_date; -- 2. Select all orders, sort by required_date (descending) and sort by shipped_date (ascending)



SELECT MIN(units_in_stock)
FROM products
WHERE discontinued != 1 AND units_in_stock > 30; -- 3. Select the minimum number of items among those products that are on sale more than 30 units.


SELECT MAX(units_in_stock)
FROM products
WHERE discontinued != 1 AND units_in_stock > 30; -- 4. Select the maximum number of items among those products that are on sale more than 30 units.


SELECT AVG(shipped_date - order_date)
FROM orders
WHERE ship_country = 'USA'; -- 5. Find the average value of the days of delivery from the date of formation of the order in USA


SELECT SUM(unit_price * units_in_stock)
FROM products
WHERE discontinued != 1; --6. Find the amount of goods you have (quantity * price) that you plan to sell in the future (see discontinued).
