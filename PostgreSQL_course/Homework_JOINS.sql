-- HOMEWORK 'JOINS'
SELECT *
FROM employees;

--1. Find customers and employees who serve their orders such that both customers and employees are from London 
--and the delivery is by Speedy Express. Output the customer's company and the employee's name.

SELECT customers.company_name AS customer,
	  employees.first_name || ' ' || employees.last_name AS employee
FROM orders
INNER JOIN employees ON orders.employee_id = employees.employee_id
INNER JOIN customers ON orders.customer_id = customers.customer_id
INNER JOIN shippers ON orders.ship_via = shippers.shipper_id
WHERE customers.city = 'London' AND employees.city = 'London' AND shippers.company_name = 'Speedy Express'; 


--2. Find active (see discontinued field) products from Beverages and Seafood that are less than 20 units on sale. 
--Display the name of the product, the number of units on sale, the name of the supplier's contact, and his phone number.


SELECT products.product_name, products.units_in_stock, suppliers.contact_name, suppliers.phone
FROM products
JOIN categories USING(category_id)
JOIN suppliers USING(supplier_id)
WHERE discontinued != 1 and category_name IN ('Beverages', 'Seafood') and units_in_stock < 20;


--3. Find customers who have not placed any orders. Output the customer name and order_id.

SELECT company_name, contact_name,  order_id
FROM customers
LEFT JOIN orders USING(customer_id)
WHERE order_id IS NULL;


--4. Rewrite the previous query using a symmetric join (hint: it is about LEFT and RIGHT).

SELECT company_name, contact_name,  order_id
FROM orders
RIGHT JOIN customers USING(customer_id)
WHERE order_id IS NULL;
