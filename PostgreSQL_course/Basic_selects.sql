-- LESSON 1: SELECT, DISTINCT, COUNT
SELECT * FROM products; -- select all columns from 'product' table

SELECT product_id, product_name, unit_price FROM products; -- select columns:product_id,product_name, unit_price from product table

SELECT product_id, unit_price * units_in_stock FROM products; -- selecet product_id column and multiplie columns unit_price and _units_in_stock from products table

SELECT DISTINCT country FROM employees; -- select only unique elements in column country from employees table

SELECT DISTINCT country, city FROM employees; -- select unique pairs country-city in columns country and city from employees table

SELECT COUNT(*) FROM orders; -- count all rows in table orders;

SELECT COUNT(DISTINCT country) FROM employees; -- count only unique coutries in column country from employees table

--Homework 1

SELECT * FROM customers; -- 1. Select all data from the table

SELECT contact_name, city FROM customers; -- 2. Select all records from the customers table, but only the "contact name" and "city" columns

SELECT order_id, shipped_date - order_date FROM orders; -- 3. Select all records from the table orders, 
--but take two columns: the order ID and the column whose value we calculate as the difference between the date of shipment and the date of formation of the order.

SELECT DISTINCT city FROM customers; -- 4. Select all unique cities in which customers are "registered".

SELECT DISTINCT city, country FROM customers; -- 5. Select all unique combinations of cities and countries where customers are "registered".

SELECT COUNT(customer_id) FROM customers; -- 6. Count the number of customers

SELECT COUNT(DISTINCT country) FROM customers -- 7. Count the number of unique countries where customers are registered
