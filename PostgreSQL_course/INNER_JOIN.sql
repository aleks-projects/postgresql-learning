--LESSON 8
-- INNER JOIN


SELECT product_name, suppliers.company_name, units_in_stock
FROM products
INNER JOIN suppliers ON products.supplier_id = suppliers.supplier_id
ORDER BY units_in_stock DESC; -- select columns product_name and units_in_stock from products_table and column company_name from table suppliers
-- combining tables products and suppliers using supplier_id and INNER JOIN 


SELECT category_name, SUM(units_in_stock)
FROM products
INNER JOIN categories ON products.category_id = categories.category_id
GROUP BY category_name
ORDER BY SUM(units_in_stock) DESC; 

SELECT category_name, SUM(units_in_stock * unit_price)
FROM products
INNER JOIN categories ON products.category_id = categories.category_id
WHERE products.discontinued != 1
GROUP BY category_name
HAVING SUM(units_in_stock * unit_price) > 5000
ORDER BY SUM(units_in_stock * unit_price) DESC
LIMIT 10;



SELECT first_name, last_name, order_date, ship_country, ship_city
FROM employees
INNER JOIN orders ON employees.employee_id = orders.employee_id
ORDER BY ship_country ASC, order_date DESC ;


SELECT order_date, ship_country, product_name, products.unit_price, quantity, discount
FROM orders
INNER JOIN order_details ON orders.order_id = order_details.order_id
INNER JOIN products ON order_details.product_id = products.product_id; -- select columns order_date, ship_country from table orders
-- product_name, unit_price from table products, quantity, discount from table order_details combining tables orders and orders_details using order_id
--and combining tables products and order_details using product_id


SELECT contact_name, company_name, phone, first_name, last_name, title,
	order_date, product_name, ship_country, products.unit_price, quantity, discount
FROM orders
JOIN order_details ON orders.order_id = order_details.order_id
JOIN products ON order_details.product_id = products.product_id
JOIN customers ON orders.customer_id = customers.customer_id
JOIN employees ON orders.employee_id = employees.employee_id
WHERE ship_country = 'USA'; 
