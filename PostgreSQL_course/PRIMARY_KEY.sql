-- Lesson 14
-- PRIMARY KEY 


CREATE TABLE faculty
(
	faculty_id serial, --PRIMARY KEY,
	faculty_name varchar(256),
	dean varchar(128),

	CONSTRAINT PK_faculty_faculty_id PRIMARY KEY(faculty_id) -- same as the record: faculty_id serial PRIMARY KEY
);

SELECT * FROM faculty; -- select all data from table faculty

CREATE TABLE faculty
(
	faculty_id serial UNIQUE NOT NULL ,
	faculty_name varchar(256),
	dean varchar(128)
);  -- create table faculty with 3 columns faculty_id which can include only unique values, faculty_name, and dean

INSERT INTO faculty 
VALUES
(1, 'name' , 'dean_1'),
(2, 'name_2', 'dean_2');-- add data to faculty table

-- UNIQUE says that in the column can be only unique values
-- PRIMARY KEY is used to identify unique row in the table
-- In the table can be only one PRIMARY KEY column but a lot of UNIQUE NOT NULL columns

DROP TABLE faculty; -- delete faculty table

SELECT constraint_name
FROM information_schema.key_column_usage
WHERE table_name = 'faculty' AND table_schema = 'public' AND column_name = 'faculty_id'; -- select constraint name 
-- for table faculty column faculty_id

ALTER TABLE faculty
DROP CONSTRAINT pk_faculty_faculty_id; -- drop constraint in faculty table column faculty_id


ALTER TABLE faculty
ADD CONSTRAINT pk_faculty_faculty_id PRIMARY KEY(faculty_id); -- add  constraint in faculty table column faculty_id
