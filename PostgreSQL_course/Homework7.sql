--Homework 7
-- "VIEWs" Homework


CREATE VIEW orders_customers_employees AS
SELECT order_date, required_date, shipped_date, ship_postal_code, company_name,
contact_name, phone, last_name, first_name, title
FROM orders
JOIN customers USING(customer_id)
JOIN employees USING(employee_id);


SELECT *
FROM orders_customers_employees
WHERE order_date > '1997-01-01'
ORDER BY order_date ; -- 1. Create a view that outputs the following columns:

--order_date, required_date, shipped_date, ship_postal_code, company_name, contact_name, phone, last_name, first_name, 
--title from the orders, customers, and employees tables.
--Make a select to the created view, bringing up all records where order_date is greater than January 1, 1997.


CREATE VIEW products_view AS
SELECT *
FROM products
WHERE discontinued =0
WITH LOCAL CHECK OPTION;


INSERT INTO products_view
VALUES
(81,'Bananas', 16,4,'1 kg',30,100,20,1,1);--3.  Create a view of "active" (discontinued = 0) products containing
--all columns. The view must be protected from inserting records with discontinued = 1.
--Ty inserting a record with discontinued = 1 - see if it fails
