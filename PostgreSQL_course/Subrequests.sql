--LESON 12:
--Subrequests 

SELECT company_name
FROM suppliers
WHERE country IN (SELECT DISTINCT country 
				 FROM customers); -- select column company name from suppliers table where  country 
-- in suppliers table  belong to contries from customers table
				 
				 
SELECT DISTINCT suppliers.company_name
FROM suppliers
JOIN customers USING(country); -- same request as above



SELECT category_name, SUM(units_in_stock) AS units_sum
FROM products 
JOIN categories USING(category_id)
GROUP BY category_name
ORDER BY SUM(units_in_stock) DESC
LIMIT(SELECT MIN(product_id)+4
	 FROM products);-- select category_name from categories table and units_sum column from tables products and categories 
	 -- using inner join category_id column sorted units_sum column in descending order limit request by number
	 --which is calculated as min value in product_id column increased by 4
	 

SELECT product_name, units_in_stock
FROM products
WHERE units_in_stock > (SELECT AVG(units_in_stock)
					   FROM products)
ORDER BY units_in_stock ASC; -- select columns product_name and units_in_stock from table products 
--where units_in_stock is greaten than average value in units_in_stock column 
--and sorderd column units_in_stock on ascending order



