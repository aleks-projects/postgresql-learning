--Lesson 17
-- INSERT, UPDATE, DELETE , RETURNING

CREATE TABLE autohor
(
	author_id int GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT 1) NOT NULL,
	full_name text,
	rating float,
	
	CONSTRAINT PK_author_author_id PRIMARY KEY(author_id)
);
SELECT * FROM author;

INSERT INTO author (full_name, rating)
VALUES
('JOHN SILVER', 4.5),
('DAVID BRANCH', 5.0); -- insert data to author table to columns full_name and rating


SELECT *
INTO best_authors
FROM author
WHERE rating >4.5; -- this syntax can be run only one time

SELECT *
FROM best_authors;


INSERT INTO best_authors
SELECT  *
FROM author
WHERE rating =4.5; -- insert data to best_authors table selected from author table 



-- UPDATE  DELETE 

SELECT * FROM author;


UPDATE author
SET full_name = 'Bias', rating = 3.5
WHERE author_id = 2; -- to change data at author table in row with author_2  equal to 2  at columns full_name and rating 


DELETE FROM author
WHERE rating = 4.5; -- to delete rows at author table in which rating is equal to 4.5

DELETE FROM author; -- to delete all rows 




-- RETURNING 


CREATE TABLE book 
(
	book_id serial,
	title text NOT NULL,
	isbn varchar(32) NOT NULL,
	publisher_id int NOT NULL,
	
	CONSTRAINT PK_book_book_id PRIMARY KEY(book_id)		
); 


INSERT INTO book (title,isbn,publisher_id)
VALUES
('title', 'isbn', 3),
('title2', 'isbn2', 4)
RETURNING book_id; -- after insert data to columns return inserted data from column book_id


INSERT INTO book (title,isbn,publisher_id)
VALUES
('title', 'isbn', 3),
('title2', 'isbn2', 4)
RETURNING *; -- after insert data to columns return all inserted data.


--RETURNING can use with DELETING AND UPDATING 
UPDATE book
SET title = 'Harry Potter', isbn = '124124', publisher_id = 5
WHERE book_id = 1
RETURNING *;
