--LESSON 6:
-- GROUP BY, HAVING

SELECT ship_country, COUNT(*)
FROM orders
WHERE freight > 50
GROUP BY ship_country
ORDER BY COUNT(*) DESC; -- select column ship_country from table orders where freight is greaten than 50 
--and count all rows where the freight is greater than 50 grouping it by ship_country and sort it descending order 


SELECT category_id, SUM(units_in_stock)
FROM products
GROUP BY category_id; -- select column category_id from table products and count sum of units_in_stock grouping it by category_id


SELECT category_id, SUM(units_in_stock * unit_price)
FROM products
WHERE discontinued != 1
GROUP BY category_id
HAVING SUM(units_in_stock * unit_price) > 10000
ORDER BY SUM(units_in_stock * unit_price) DESC; -- select column category_id from table products and count sum of multipied columns:
-- unit_in_stock and unit_price groping it by category_id and return only theese columns where this sum is greaten than 10000 and sort it descending order 


SELECT ship_city, COUNT(*)
FROM orders
GROUP BY ship_city; --select column ship_city from table orders and count all rows groping by ship_city
