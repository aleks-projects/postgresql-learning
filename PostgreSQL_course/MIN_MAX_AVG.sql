SELECT ship_city, order_date
FROM orders
WHERE ship_city ='London'
ORDER BY order_date ASC; -- select columns:ship_city, order_date from table orders  where city is London
--and sort order_date column in ascending order

SELECT MIN(order_date)
FROM orders
WHERE ship_city ='London'; -- select min order_date from table orders



SELECT ship_city, order_date
FROM orders
WHERE ship_city ='London'
ORDER BY order_date DESC; -- select columns:ship_city, order_date from table orders  where city is London
--and sort order_date column in descending order

SELECT MAX(order_date)
FROM orders
WHERE ship_city ='London'; -- select max order_date from table orders

SELECT * FROM products;

SELECT AVG(unit_price)
FROM products
WHERE discontinued != 1; -- calculate the average from the column unit_price from table products where discontinued is not equal 1 
