--Lesson 17
-- 'VIEW', DELETE, ALTER VIEW, 


CREATE VIEW products_suppliers_categories AS
SELECT product_name, quantity_per_unit, unit_price, units_in_stock, company_name, phone,category_name, description
FROM products
JOIN categories USING(category_id)
JOIN suppliers USING(supplier_id); -- create VIEW named products_suppliers_categories which includes 3 joined tables
--products, categories, suppliers and columns such as product_name, quantity_per_unit,
--unit_price, units_in_stock, company_name, phone,category_name, description


SELECT *
FROM products_suppliers_categories; -- select all data from products_suppliers_categories 'VIEW'



SELECT *
FROM products_suppliers_categories
WHERE unit_price > 20;-- select all data from products_suppliers_categories 'VIEW' where unit_price is greater than 20


DROP VIEW products_suppliers_categories; -- drop 'VIEW' products_suppliers_categories



SELECT *
FROM orders;


CREATE VIEW heavy_orders AS
SELECT *
FROM orders
WHERE freight > 50; -- create "VIEW" witch include all columns from orders table and where freight is greater than 50



SELECT *
FROM heavy_orders
ORDER BY freight; -- select all data from heavy_orders 'VIEW' sorted in ascending order using freight column


CREATE OR REPLACE VIEW heavy_orders AS
SELECT *
FROM orders
WHERE freight > 100; -- create in not exist or replace if exists heavy_orders 'VIEW'



-- It is not possible to alter 'VIEW' by adding new columns !!!


ALTER VIEW products_suppliers_categories RENAME TO psc_old; -- rename "VIEW" products_suppliers_categories to psc_old


SELECT MAX(order_id)
FROM orders; -- select max order_id at orders table


INSERT INTO heavy_orders
VALUES 
(11078,'VINET',5,'2019-12-10','2019-12-15','2019-12-14',1,120,'Heanary Carnes',
 'Rua do Paco','Bern',null,3012,'Switzerland'); -- add data to 'VIEW' heavy_orders


SELECT *
FROM heavy_orders
ORDER BY order_id DESC; -- select all data from heavy_orders 'VIEW' sorted in descending order using order_id  column



SELECT MIN(freight)
FROM orders; -- select min freight in orders table

DELETE FROM heavy_orders
WHERE freight < 0.05; -- delete rows in heavy_orders "VIEW" where freight is less than 0.05, it is not possible to delete
--date which is not in "VIEW" but is in table



INSERT INTO heavy_orders
VALUES
(11900, 'FOLIG', 1, '2000-01-01', '2000-01-05', '2000-01-04', 1, 80, 'Folies gourmandes','184 chaussee de Tournai',
	'Lille',NULL,59000,'France'); -- we can insert data to "VIEW" heavy_orders which freight equal to 80 which contradicts
-- how we created 'VIEW'

SELECT *
FROM heavy_orders
WHERE order_id = 11900; -- It is not possible to find row whitch we insert above in view but we can find it in table

SELECT *
FROM orders
WHERE order_id = 11900;



-- During creating "VIEW" we can add WITH LOCAL CHECK OPTION and this command 
