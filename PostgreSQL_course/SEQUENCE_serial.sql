-- Lesson 16
--SEQUENCE, serial

CREATE SEQUENCE seq1; -- create sequence named seq1

SELECT nextval('seq1'); -- call next  iteration
SELECT currval('seq1'); -- print actual value
SELECT lastval(); -- print last  value at any sequance in actual session

SELECT setval('seq1', 16, true); -- set value 16 for sequency 'seq1'
SELECT currval('seq1');
SELECT nextval('seq1');


SELECT setval('seq1', 6, false); -- set value 6 for sequence 'seq1' whitch will call in next iteration
SELECT currval('seq1');
SELECT nextval('seq1');


CREATE SEQUENCE IF NOT EXISTS seq2 INCREMENT 16;
SELECT nextval('seq2'); -- create sequence named seq2 with increment 16, first value is default 1


CREATE SEQUENCE IF NOT EXISTS seq3
INCREMENT 16
MINVALUE 0
MAXVALUE 128
START WITH 0; -- create sequence named seq3 which start at 0 and end achive value 128 

SELECT nextval('seq3');


ALTER SEQUENCE seq3 RENAME TO seq4; -- rename sequence seq3 to seq4 
ALTER SEQUENCE seq4 RESTART WITH 16; -- restart seq4 at 16

SELECT nextval('seq4');

DROP SEQUENCE seq4; -- delete seq4



DROP TABLE IF EXISTS book;

CREATE TABLE book 
(
	book_id int NOT NULL,
	title text NOT NULL,
	isbn varchar(32) NOT NULL,
	publisher_id int NOT NULL,
	
	CONSTRAINT PK_book_book_id PRIMARY KEY(book_id)
		
);

CREATE SEQUENCE IF NOT EXISTS book_book_id_seq
START WITH 1 OWNED BY book.book_id; -- create sequence named book_book_id_seq and assign it to book_id column in book table

ALTER TABLE book
ALTER COLUMN book_id SET DEFAULT nextval('book_book_id_seq'); -- default constaint call nextval() function everytime 
-- when during insert data column book_id is skiped

INSERT INTO book(title, isbn, publisher_id)
VALUES
('title','isbn',1);

SELECT * FROM book;


-- serial type is same that integer but with auto increment 

CREATE TABLE book 
(
	book_id serial NOT NULL,
	title text NOT NULL,
	isbn varchar(32) NOT NULL,
	publisher_id int NOT NULL,
	
	CONSTRAINT PK_book_book_id PRIMARY KEY(book_id)		
); -- using serial you can manually write data to book_id column


INSERT INTO book(title, isbn, publisher_id)
VALUES
('title','isbn',1);

SELECT * FROM book;



CREATE TABLE book 
(
	book_id int GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT 1) NOT NULL,
	title text NOT NULL,
	isbn varchar(32) NOT NULL,
	publisher_id int NOT NULL,
	
	CONSTRAINT PK_book_book_id PRIMARY KEY(book_id)
		
); -- in this way you cant manually write data to book_id column 

INSERT INTO book(title, isbn, publisher_id)
VALUES
('title','isbn',1);
