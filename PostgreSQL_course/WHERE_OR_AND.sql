--LESSON 2:
--WHERE, AND, OR

SELECT company_name, city, country
FROM customers
WHERE country='USA'; -- select all rows in columns company_name, city, country from table where coutry is USA


SELECT *
FROM products
WHERE unit_price >20; -- select all data where unite_price is greater than 20

SELECT COUNT(product_id)
FROM products
WHERE unit_price >20; -- count how many product price is greaten than 20

SELECT * 
FROM products
WHERE discontinued = 1; --select all data where column discontinued is equal to 1


SELECT company_name, address, city
FROM customers
WHERE city != 'London'; -- select columns: company_name, address, city, where city is not equal London 


SELECT *
FROM orders
WHERE order_date > '1998-01-01'; -- select all columns form table orders where order_date is greater than 1998-01-01


--AND, OR

SELECT product_name, product_id, unit_price,units_in_stock
FROM products
WHERE unit_price > 25 AND units_in_stock > 40; -- select columns product_name, product_id, unit_price,units_in_stock fro table products 
-- where unit_price is greater than 25 and units_in_stock is greater than 40


SELECT contact_name, city, address
FROM customers
WHERE city = 'Berlin' OR city = 'London' OR city = 'Madrid'; -- select columns contact_name, city, address from table customers 
--where city is Berlin or London or Madrid

SELECT *
FROM orders
WHERE shipped_date > '1998-04-30' and (freight < 75 or freight > 150); -- select all columns from table orders where
--after 1998-04-30 and freight is less than 75 or greaten than 150
