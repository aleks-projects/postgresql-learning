CREATE TABLE employee(
 employee_id INT PRIMARY KEY,
 first_name varchar(128) NOT NULL,
 last_name varchar(128) NOT NULL,
 manager_id INT,
 FOREIGN KEY (manager_id) REFERENCES employee(employee_id)
); -- create table employee with columns employee_id, first_name, last_name, manager_id


INSERT INTO employee(
	employee_id,
	first_name,
	last_name,
	manager_id
)
VALUES
(1,'WINDY', 'Hays', NULL),
(2,'Ava','Christensen',1),
(3,'Hassan','Conner',1),
(4,'Anna','Reeves',2),
(5,'Sau','Norman',2),
(6,'Kelsie','Hays',3),
(7,'Tory','Groff',3),
(8,'Salley','Lester',3);


SELECT *
FROM employee;


SELECT e.first_name || ' ' || e.last_name AS employee,
	   m.first_name || ' ' || m.last_name AS manager
FROM employee e
LEFT JOIN employee m ON m.employee_id = e.manager_id
ORDER BY manager;



