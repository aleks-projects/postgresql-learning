--LESSON 15
-- FOREIGN KEY


CREATE TABLE publisher
(
	publisher_id int,
	publisher_name varchar(128) NOT NULL,
	address text,
	
	CONSTRAINT PK_publisher_publisher_id PRIMARY KEY(publisher_id)
); -- create table publisher with columns publisher_id as a PRIMARY KEY column, publisher_name, address

CREATE TABLE book
(
	book_id int,
	title text NOT NULL,
	isbn varchar(23) NOT NULL,
	publisher_id int,
	
	CONSTRAINT PK_book_book_id PRIMARY KEY(book_id),
	CONSTRAINT FG_books_publisher FOREIGN KEY(publisher_id) REFERENCES publisher(publisher_id)
); -- create table book with columns book_id as PRIMARY KEY, title isbn, publisher_id as FOREIGN KEY


INSERT INTO publisher
VALUES
(1,'Everyman''s Library', 'NY'),
(2,'Oxford University Press', 'NY'),
(3,'Grand Central Publishing', 'Washington'),
(4,'Simon & Schuster', 'Chicago'); -- insert data to publisher table

INSERT INTO book
VALUES
(1,'Harry Potter', '123534614',1),
(2,'Baby Yoda', '11111111',2),
(3,'Din, Don', '1000000',4),
(4,'Hi, People', '9999999',1),
(5,'Good day', '12452363',NULL);



SELECT * FROM book;
SELECT * FROM publisher;



SELECT title, publisher_name
FROM book
INNER JOIN publisher ON  publisher.publisher_id = book.publisher_id;


SELECT title, publisher_name
FROM book
LEFT JOIN publisher ON  publisher.publisher_id = book.publisher_id;


SELECT title, publisher_name
FROM book
RIGHT JOIN publisher ON  publisher.publisher_id = book.publisher_id;


SELECT title, publisher_name
FROM book
FULL JOIN publisher ON  publisher.publisher_id = book.publisher_id;
