-- Lesson 19
-- SQL functions, Pl/pgSQL functions



SELECT *
INTO tmp_customers
FROM customers; -- to copy all data from table customers to table tmp_customers;


SELECT *
FROM tmp_customers;

CREATE OR REPLACE FUNCTION fix_customer_region() RETURNS void AS 
	$$
	UPDATE tmp_customers
	SET region = 'unknown'
	WHERE region IS NULL
	$$
LANGUAGE SQL; -- createing function fix_customer_region which does not charge any arguments 
-- and does not return anything but change table tmp_customers by changing NULL value in column region to 'unknown'


SELECT fix_customer_region(); -- call fix_customer_region





CREATE OR REPLACE FUNCTION units_sum() RETURNS bigint AS
$$
SELECT SUM(units_in_stock)
FROM products
$$
LANGUAGE SQL; -- create function units_sum() which count sum of units_in_stock column


SELECT units_sum() AS total_units;


CREATE OR REPLACE FUNCTION average_price() RETURNS decimal AS
$$
SELECT AVG(unit_price)
FROM products
$$
LANGUAGE SQL;-- create function average_price()  which count average of unit_price column

SELECT average_price();


CREATE OR REPLACE FUNCTION get_price_for_name(p_name varchar) RETURNS real AS
$$
SELECT unit_price
FROM products
WHERE product_name = p_name
$$
LANGUAGE SQL; -- function get_price_for_name() which charge product_name as argument and return unit_price



SELECT get_price_for_name('Chocolade') AS price;



CREATE OR REPLACE FUNCTION get_price_borders(OUT min_price real, OUT real_max real) AS
$$
SELECT MIN(unit_price), MAX(unit_price)
FROM products

$$
LANGUAGE SQL; -- function which return min_price and max_price from table products



SELECT get_price_borders();
SELECT * FROM get_price_borders();


CREATE OR REPLACE FUNCTION get_price_borders_use_discontinued(disc int DEFAULT 1, OUT min_price real, OUT real_max real) AS
$$
SELECT MIN(unit_price), MAX(unit_price)
FROM products
WHERE discontinued = disc 
$$
LANGUAGE SQL; -- function get_price_borders_use_discontinued() returns min_price and max_price from table products
-- where discontinued is equal disc(user argument) but disc is default 1


SELECT get_price_borders_use_discontinued();
SELECT * FROM get_price_borders_use_discontinued();
