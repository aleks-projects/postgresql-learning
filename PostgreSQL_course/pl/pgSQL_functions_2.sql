--Lesson 22
-- pl/pgSQL functions IF, ELSIF, ELSE, WHILE, WHILE, FOR


DROP FUNCTION convert_temp;
CREATE OR REPLACE FUNCTION convert_temp(temperature real, flaga bool DEFAULT true) RETURNS real AS
$$
BEGIN
	IF flaga = true THEN
		RETURN (5.0/9.0)*(temperature-32);
	ELSE
		RETURN (9*temperature+(32*5))/5;
	END IF;
END;
$$ LANGUAGE plpgsql;


SELECT convert_temp(80);
SELECT convert_temp(26.7,false);


CREATE OR REPLACE FUNCTION month_name(month_num int DEFAULT 1) RETURNS varchar AS
$$
DECLARE
	month_name varchar;
BEGIN
	IF (month_num BETWEEN 1 AND 2) OR (month_num = 12) THEN
		season:= 'Winter';
	ELSIF month_num BETWEEN 3 AND 5 THEN
		season := 'Spring';
	ELSIF month_num BETWEEN 6 AND 8 THEN
		season := 'Summer';
	ELSIF  month_num BETWEEN 9 AND 1 THEN
		season := 'Autumn';
	ELSE
		season := 'unknown';
	END IF;
	RETURN season;
END;
$$ LANGUAGE plpgsql;


SELECT season(5);


-----------------LOOPS

CREATE OR REPLACE FUNCTION fibonaci(n int) RETURNS int AS
$$
DECLARE
	num_1 int := 0;
	num_2 int := 1;
	counter int := 0;
BEGIN
	IF n<1 THEN
		RETURN 0;
	END IF;
	
	WHILE counter < n
		LOOP
			counter := counter + 1;
			SELECT num_2, num_1+num_2 INTO num_1, num_2;
		END LOOP;
	RETURN num_2;
END;
$$ LANGUAGE plpgsql;


SELECT fibonaci(4);


DO $$
BEGIN
	FOR counter IN 1..5
	LOOP
		RAISE NOTICE 'Counter: %', counter;
	END LOOP;
END $$;
