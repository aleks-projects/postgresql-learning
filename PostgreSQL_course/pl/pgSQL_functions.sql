-- Lesson 21
--Pl/pgSQL functions: BEGIN, END, DECLARE 


CREATE OR REPLACE FUNCTION units_sum() RETURNS bigint AS  $$
BEGIN
	RETURN SUM(units_in_stock)
	FROM products;
END;
$$
LANGUAGE plpgsql; -- simple Pl/pgSQL function which return sum of all products

SELECT units_sum();



CREATE OR REPLACE FUNCTION get_max_price_for_discontinued() RETURNS real AS  $$
BEGIN
	RETURN MAX(unit_price)
	FROM products
	WHERE discontinued =1;
END;
$$
LANGUAGE plpgsql; 

SELECT get_max_price_for_discontinued();


CREATE OR REPLACE FUNCTION get_price_boundries(OUT min_price real, OUT max_price real) AS $$
BEGIN
--min_price := MAX(unit_price) FROM products;
--max_price := MIN(unit_price) FROM products; OR

SELECT MIN(unit_price), MAX(unit_price)
INTO min_price, max_price
FROM products;
END;
$$ LANGUAGE plpgsql;

SELECT get_price_boundries();




CREATE OR REPLACE FUNCTION int_sum(num1 int, num2 int, OUT int_sum smallint) AS 
$$
BEGIN
int_sum := num1 + num2;
RETURN;
END;
$$
LANGUAGE plpgsql

SELECT int_sum(10, 20);




CREATE OR REPLACE FUNCTION get_clients_from_country(countr varchar) RETURNS SETOF customers AS 
$$
BEGIN
	RETURN QUERY 
	SELECT *
	FROM customers
	WHERE country = countr;
END;
$$ LANGUAGE plpgsql;


SELECT contact_name, country
FROM get_clients_from_country('USA');




CREATE FUNCTION triangle_square(b real, d real, e real) RETURNS real AS
$$
DECLARE
	p_1 real;
BEGIN
	p_1 = (b+d+e)/2;
	RETURN sqrt(p_1 * (p_1-b) * (p_1-d) * (p_1-e));
END;
$$ LANGUAGE plpgsql;


SELECT triangle_square(1,3,3);





CREATE FUNCTION middle_price() RETURNS SETOF products AS 
$$
DECLARE
	low_price real;
	high_price real;
	avg_price real;
BEGIN
	SELECT AVG(unit_price) INTO avg_price
	FROM products;

	low_price := avg_price * 0.75;
	high_price := avg_price *1.25;

	RETURN QUERY
	SELECT *
	FROM products
	WHERE unit_price BETWEEN low_price AND high_price;
END;
$$ LANGUAGE plpgsql;


SELECT product_name, unit_price
FROM middle_price();
