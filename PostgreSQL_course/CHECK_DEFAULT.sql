-- LESSON 16
-- CHECK, DEFAULT, 


CREATE TABLE book
(
	book_id int,
	title text NOT NULL,
	isbn varchar(23) NOT NULL,
	publisher_id int,
	
	CONSTRAINT PK_book_book_id PRIMARY KEY(book_id)
	
); -- create table book with columns book_id as PRIMARY KEY, title isbn, publisher_id 


ALTER TABLE book
ADD CONSTRAINT FK_books_publisher FOREIGN KEY(publisher_id) REFERENCES publisher(publisher_id); -- add constraint
--for column publisher_id as FOREIGN KEY


ALTER TABLE book
ADD COLUMN price decimal CONSTRAINT CHK_book_price CHECK (price > 0 AND price <100); --add column price to book table with 
-- constraint which check if price is greater than 0 and less than 100 if not server will print error

INSERT INTO book
VALUES
(1,'Harry Potter','123456789', 1, 10);

SELECT * FROM book;





DROP table customer;

CREATE TABLE customer
(
	customer_id serial,
	full_name varchar(256) NOT NULL,
	status text DEFAULT 'regular',
	
	
	CONSTRAINT PK_customer_customer_id PRIMARY KEY(customer_id),
	CONSTRAINT CHK_customer_status CHECK (status= 'regular' OR status = 'VIP')
); -- create table customer with columns customer_id with constraint as PRIMARY KEY, full_name, status with default value 
-- 'regular' and constraint whitch check if status is regular or VIP


INSERT INTO customer
VALUES
(1, 'Mike Tayson');


INSERT INTO customer
VALUES
(2, 'Cristian Ronaldo', 'VIP'),
(3, 'Leonel Messi', 'regular');


SELECT * FROM customer;


ALTER TABLE customer
ALTER COLUMN status DROP DEFAULT; -- to delete default option

ALTER TABLE customer
ALTER COLUMN status SET DEFAULT 'regular'
