--LESSON 9
--LEFT, RIGHT, JOIN
-- INNER JOIN = JOIN

SELECT company_name, product_name
FROM suppliers
JOIN products ON suppliers.supplier_id = products.supplier_id;


SELECT company_name, order_id
FROM customers
INNER JOIN orders ON customers.customer_id = orders.customer_id
WHERE order_id IS NULL;

SELECT company_name, order_id
FROM customers
LEFT JOIN orders ON customers.customer_id = orders.customer_id
WHERE order_id IS NULL;

SELECT company_name, order_id
FROM customers
RIGHT JOIN orders ON customers.customer_id = orders.customer_id
WHERE order_id IS NULL;


SELECT first_name, last_name, title, orders.employee_id
FROM employees
LEFT JOIN orders ON  orders.employee_id =  employees.employee_id;


SELECT product_name, unit_price, company_name
FROM products
LEFT JOIN suppliers ON suppliers.supplier_id = products.supplier_id;
