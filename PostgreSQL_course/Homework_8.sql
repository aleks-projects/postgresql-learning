--Homework 8
-- Conditions WHEN CASE, function COALESCE, NULLIF
INSERT INTO customers(customer_id, contact_name, city, country, company_name)
VALUES
('AAAAA', 'Alfred Mann', NULL, 'USA', 'fake_company'),
('BBBBB', 'Alfred Mann', NULL, 'Austria','fake_company');



SELECT *
FROM customers;


SELECT contact_name, city, country
FROM customers
ORDER BY contact_name, ( 
						CASE
						WHEN city IS NULL THEN country
						ELSE city
						END
						); -- 1. Print the customer's contact name, city and country, sorted in ascending order by
						--contact name and city,
						--and if the city is NULL, then by contact name and country. 
						--Check the result using the pre-pasted rows. 
						
						
SELECT product_name, unit_price,
	CASE
	WHEN unit_price >= 100 THEN 'too expensive'
	WHEN unit_price >= 50 AND unit_price < 100 THEN 'average'
	WHEN unit_price < 50 THEN 'low price'
	END AS price_description
FROM products
ORDER BY unit_price; -- 2. SELECT  product_name, product price and the column with the values
							--too expensive if price >= 100
							--average price if price >= 50 but < 100
							--low price if price < 50
							
SELECT * 
FROM orders;

SELECT DISTINCT contact_name, COALESCE(TO_CHAR(order_id, 'FM99999999'), 'no orders') AS order_id
FROM customers
LEFT JOIN orders USING(customer_id)
WHERE order_id IS NULL; --3. Find customers who have not placed any orders. 
--Print the customer name and the value 'no orders' if order_id = NULL.



SELECT first_name || ' ' || last_name AS full_name,
			COALESCE(NULLIF(title,'Sales Representative'), 'Sales Stuff') AS title
FROM employees;--4. Select names of employees and their positions. 
				--If position = Sales Representative, output Sales Stuff instead.



