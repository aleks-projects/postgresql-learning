--LESSON 13
--DDL
-- CREATE TABLE table_name - to create table
-- ALTER TABLE table_name:  -- to change table
      --ADD COLUMN column_name data_type - to add column to table
	  -- RENAME TO new_table_name - to rename table
	  -- RENAME old_column_name TO new_column_name - to rename column
	  -- ALTER COLUMN column_name SET DATA TYPE data_type - to change data type in column
-- DROP TABLE table_name - to delete table
-- TRUNCATE TABLE table_name - to clear all data in table, but cant clear data
                               --which is limited byl foreign key from other table
-- DROP COLUMN column_name - to delete column


CREATE TABLE student
(
	student_id serial,
    first_name varchar,
	last_name varchar,
	birthday date,
	phone varchar
);-- creating table student with columns student_id, first_name, last_name itd

CREATE TABLE cathedra 
(
	cathedra_id serial,
	cathedra_name varchar,
	dean varchar
);-- creating table cathedra with columns cathedra_id, cathedra_name, dean


ALTER TABLE student
ADD COLUMN middle_name varchar;


ALTER TABLE student
ADD COLUMN rating float;

ALTER TABLE student
ADD COLUMN enrolled date; -- add 3 columns named middle_name, rating, enrolled to student table


ALTER TABLE student
DROP COLUMN middle_name; -- delete column middle_name 


ALTER TABLE cathedra
RENAME TO faculty; -- rename table cathedra to faculty


ALTER TABLE faculty
RENAME cathedra_id TO faculty_id;



ALTER TABLE faculty
RENAME cathedra_name TO faculty_name; -- rename columns cathedra_id and cathedra_name to faculty_id and faculty_name



ALTER TABLE student
ALTER COLUMN first_name SET DATA TYPE varchar(64),
ALTER COLUMN last_name SET DATA TYPE varchar(64),
ALTER COLUMN phone SET DATA TYPE varchar(30); -- change data type for fist_name, last_name, phone columns


CREATE TABLE chair
(
	chair_id serial,
	chair_name varchar

); -- create table chair


INSERT INTO chair
VALUES
(1,'chair_1'),
(2,'chair_2'),
(3,'chair_3'); -- add data to table chair


SELECT * FROM chair; -- select all data from table chair

TRUNCATE TABLE chair; -- clear all data from table chair

--TRUNCATE TABLE table_name - clear all date without indentity (mean chair_id) 
--if during inserting data chair_id was skipped
-- TRUNCATE TABLE table_name RESTART IDENTITY -- clear all date with indentity (mean chair_id) 
--if during inserting data chair_id was skipped


DROP TABLE chair; -- delete table chair
